package net.ravenix.coinsystem.bukkit.listener;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import net.ravenix.coinsystem.bukkit.BukkitCoins;
import net.ravenix.coinsystem.shared.coins.ICoinProvider;

import java.util.UUID;

public final class PubSubListener {

    @EventListener
    public void channelMessage(ChannelMessageReceiveEvent event) {
        JsonDocument jsonDocument = event.getData();

        ICoinProvider coinProvider = BukkitCoins.getInstance().getCoinProvider();

        if (event.getChannel().equalsIgnoreCase("createCoinUser")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            coinProvider.createCoinUser(uuid, false);
        }
        if (event.getChannel().equalsIgnoreCase("addCoins")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            long coins = jsonDocument.getLong("coins");
            String serverName = jsonDocument.getString("serverName");
            coinProvider.addCoins(uuid, coins, serverName, false);
        }
        if (event.getChannel().equalsIgnoreCase("removeCoins")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            long coins = jsonDocument.getLong("coins");
            String serverName = jsonDocument.getString("serverName");
            coinProvider.removeCoins(uuid, coins, serverName, false);
        }
    }

}
