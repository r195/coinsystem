package net.ravenix.coinsystem.bukkit;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import lombok.Getter;
import net.ravenix.coinsystem.bukkit.coins.CoinProvider;
import net.ravenix.coinsystem.bukkit.listener.PubSubListener;
import net.ravenix.coinsystem.shared.coins.ICoinProvider;
import net.ravenix.core.bukkit.BukkitCore;
import org.bukkit.plugin.java.JavaPlugin;

public final class BukkitCoins extends JavaPlugin {

    @Getter
    private static BukkitCoins instance;

    @Getter
    private final String prefix = "§8[§6§lC§e§loins§8] §7";

    @Getter
    private ICoinProvider coinProvider;

    @Override
    public void onEnable() {
        instance = this;

        this.coinProvider = new CoinProvider(BukkitCore.getInstance().getMySQL());

        registerChannel();
    }

    private void registerChannel() {
        CloudNetDriver.getInstance().getEventManager().registerListener(new PubSubListener());
    }

    @Override
    public void onDisable() {

    }
}
