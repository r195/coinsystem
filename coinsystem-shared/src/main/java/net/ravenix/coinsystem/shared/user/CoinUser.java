package net.ravenix.coinsystem.shared.user;

import lombok.AllArgsConstructor;

import java.text.DecimalFormat;
import java.util.UUID;

@AllArgsConstructor
public class CoinUser implements ICoinUser {

    private final DecimalFormat decimalFormat = new DecimalFormat();

    private final UUID uuid;
    private long coins;

    @Override
    public UUID getUUID() {
        return this.uuid;
    }

    @Override
    public Long getCoins() {
        return this.coins;
    }

    @Override
    public String getCoinsAsDecimal() {
        return this.decimalFormat.format(getCoins()).replace(",", ".");
    }

    @Override
    public boolean hasEnoughCoins(long coins) {
        return this.coins >= coins;
    }

    @Override
    public void addCoins(long coins) {
        this.coins = this.coins+coins;
    }

    @Override
    public void removeCoins(long coins) {
        this.coins = this.coins-coins;
    }
}
