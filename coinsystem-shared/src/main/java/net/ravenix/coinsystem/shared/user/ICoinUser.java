package net.ravenix.coinsystem.shared.user;

import java.util.UUID;

public interface ICoinUser {

    UUID getUUID();

    Long getCoins();

    String getCoinsAsDecimal();

    boolean hasEnoughCoins(long coins);

    void addCoins(long coins);

    void removeCoins(long coins);
}
