package net.ravenix.coinsystem.shared.coins;

import net.ravenix.coinsystem.shared.user.ICoinUser;

import java.util.UUID;

public interface ICoinProvider {

    void createCoinUser(UUID uuid, boolean send);

    ICoinUser getCoinUser(UUID uuid);

    void addCoins(UUID uuid, long coins, String serverName, boolean send);

    void removeCoins(UUID uuid, long coins, String serverName, boolean send);

}
