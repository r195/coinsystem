package net.ravenix.coinsystem.bungee.commands;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.coinsystem.bungee.BungeeCoins;
import net.ravenix.coinsystem.shared.coins.ICoinProvider;
import net.ravenix.coinsystem.shared.user.ICoinUser;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.user.IPermissionUser;

import java.text.DecimalFormat;

public final class CoinCommand extends Command {

    public CoinCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) sender;
        ICoinProvider coinProvider = BungeeCoins.getInstance().getCoinProvider();
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();

        if (args.length == 0) {
            proxiedPlayer.sendMessage(BungeeCoins.getInstance().getPrefix()
                    + "§7Du hast: §6" + coinProvider.getCoinUser(proxiedPlayer.getUniqueId()).getCoinsAsDecimal() + " Coins§8.");
            return;
        }
        if (args.length == 1) {
            if (proxiedPlayer.hasPermission("ravenix.coins.see.other")) {
                String playerName = args[0];
                NameResult nameResult = nameStorageProvider.getNameResultByName(playerName);
                if (nameResult == null) {
                    proxiedPlayer.sendMessage(BungeeCoins.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
                    return;
                }
                ICoinUser coinUser = coinProvider.getCoinUser(nameResult.getUuid());
                IPermissionUser permissionUser = permissionProvider.getPermissionUser(nameResult.getUuid());
                proxiedPlayer.sendMessage(BungeeCoins.getInstance().getPrefix()
                        + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)
                        + nameResult.getName() + " §7hat §6" + coinUser.getCoinsAsDecimal() + " Coins§8.");
                return;
            }
        }
        if (args.length == 3) {
            if (args[0].equalsIgnoreCase("add")) {
                if (proxiedPlayer.hasPermission("ravenix.coins.add")) {
                    String playerName = args[1];
                    NameResult nameResult = nameStorageProvider.getNameResultByName(playerName);
                    if (nameResult == null) {
                        proxiedPlayer.sendMessage(BungeeCoins.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
                        return;
                    }
                    long coins;
                    try {
                        coins = Long.parseLong(args[2]);
                    } catch (NumberFormatException ignored) {
                        proxiedPlayer.sendMessage(BungeeCoins.getInstance().getPrefix() + "§cBitte gebe eine Zahl an.");
                        return;
                    }
                    IPermissionUser permissionUser = permissionProvider.getPermissionUser(nameResult.getUuid());
                    coinProvider.addCoins(nameResult.getUuid(), coins, CloudNetDriver.getInstance().getComponentName(), true);
                    proxiedPlayer.sendMessage(BungeeCoins.getInstance().getPrefix() + "§7Du hast " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)
                            + nameResult.getName() + " §6" + asDecimal(coins) + " Coins §7hinzugefügt.");
                    return;
                }
            }
            if (args[0].equalsIgnoreCase("remove")) {
                if (proxiedPlayer.hasPermission("ravenix.coins.remove")) {
                    String playerName = args[1];
                    NameResult nameResult = nameStorageProvider.getNameResultByName(playerName);
                    if (nameResult == null) {
                        proxiedPlayer.sendMessage(BungeeCoins.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
                        return;
                    }
                    long coins;
                    try {
                        coins = Long.parseLong(args[2]);
                    } catch (NumberFormatException ignored) {
                        proxiedPlayer.sendMessage(BungeeCoins.getInstance().getPrefix() + "§cBitte gebe eine Zahl an.");
                        return;
                    }
                    IPermissionUser permissionUser = permissionProvider.getPermissionUser(nameResult.getUuid());
                    coinProvider.removeCoins(nameResult.getUuid(), coins, CloudNetDriver.getInstance().getComponentName(), true);
                    proxiedPlayer.sendMessage(BungeeCoins.getInstance().getPrefix() + "§7Du hast " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)
                            + nameResult.getName() + " §6" + asDecimal(coins) + " Coins §7entfernt.");
                    return;
                }
            }
        }
        proxiedPlayer.sendMessage(BungeeCoins.getInstance().getPrefix()
                + "§7Du hast: §6" + coinProvider.getCoinUser(proxiedPlayer.getUniqueId()).getCoinsAsDecimal() + " Coins§8.");
    }

    private String asDecimal(Long coins) {
        return new DecimalFormat().format(coins).replace(",", ".");
    }
}
