package net.ravenix.coinsystem.bungee;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.ravenix.coinsystem.bungee.coins.CoinProvider;
import net.ravenix.coinsystem.bungee.commands.CoinCommand;
import net.ravenix.coinsystem.bungee.listener.PostLoginListener;
import net.ravenix.coinsystem.bungee.listener.PubSubListener;
import net.ravenix.coinsystem.shared.coins.ICoinProvider;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.bungee.mysql.MySQL;

public final class BungeeCoins extends Plugin {

    @Getter
    private static BungeeCoins instance;

    @Getter
    private final String prefix = "§8[§6§lC§e§loins§8] §7";

    @Getter
    private ICoinProvider coinProvider;

    @Override
    public void onEnable() {
        instance = this;

        this.coinProvider = new CoinProvider(BungeeCore.getInstance().getMySQL());

        loadCommandsAndListeners();
    }

    private void loadCommandsAndListeners() {
        CloudNetDriver.getInstance().getEventManager().registerListener(new PubSubListener());

        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();
        pluginManager.registerListener(this, new PostLoginListener());
        pluginManager.registerCommand(this, new CoinCommand("coins"));
        pluginManager.registerCommand(this, new CoinCommand("coin"));
    }

    @Override
    public void onDisable() {

    }
}
