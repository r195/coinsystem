package net.ravenix.coinsystem.bungee.listener;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.ravenix.coinsystem.bungee.BungeeCoins;
import net.ravenix.coinsystem.shared.coins.ICoinProvider;

public final class PostLoginListener implements Listener {

    @EventHandler
    public void postLogin(PostLoginEvent event) {
        ProxiedPlayer player = event.getPlayer();
        ICoinProvider coinProvider = BungeeCoins.getInstance().getCoinProvider();
        coinProvider.createCoinUser(player.getUniqueId(), true);
    }

}
