package net.ravenix.coinsystem.bungee.coins;

import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.coinsystem.shared.coins.ICoinProvider;
import net.ravenix.coinsystem.shared.user.CoinUser;
import net.ravenix.coinsystem.shared.user.ICoinUser;
import net.ravenix.core.bungee.mysql.MySQL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

public final class CoinProvider implements ICoinProvider {

    private final MySQL mySQL;

    private final Map<UUID, ICoinUser> coinUserMap = Maps.newHashMap();

    public CoinProvider(MySQL mySQL) {
        this.mySQL = mySQL;

        createTable();
        loadCoinUsers();
    }

    @Override
    public void createCoinUser(UUID uuid, boolean send) {
        if (getCoinUser(uuid) != null) return;

        ICoinUser coinUser = new CoinUser(uuid, 0);
        this.coinUserMap.put(uuid, coinUser);

        if (send) {
            this.mySQL.update("INSERT INTO coinUser (uuid,coins) VALUES ('" + uuid.toString() + "','0')");
            createCoinUser(uuid);
        }
    }

    private void createCoinUser(UUID uuid) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("createCoinUser", "update", jsonDocument);
    }


    @Override
    public ICoinUser getCoinUser(UUID uuid) {
        return this.coinUserMap.get(uuid);
    }

    @Override
    public void addCoins(UUID uuid, long coins, String serverName, boolean send) {
        ICoinUser coinUser = getCoinUser(uuid);
        if (coinUser == null) return;

        if (!send) {
            if (serverName.equals(CloudNetDriver.getInstance().getComponentName())) return;
            coinUser.addCoins(coins);
            return;
        }
        coinUser.addCoins(coins);
        addCoins(uuid, coins, serverName);
        this.mySQL.update("UPDATE coinUser SET coins='" + coinUser.getCoins() + "' WHERE uuid='" + coinUser.getUUID().toString() + "'");
    }

    private void addCoins(UUID uuid, long coins, String serverName) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        jsonDocument.append("coins", coins);
        jsonDocument.append("serverName", serverName);

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("addCoins", "update", jsonDocument);
    }

    private void createTable() {
        this.mySQL.update("CREATE TABLE IF NOT EXISTS coinUser (uuid varchar(36), coins TEXT)");
    }

    @Override
    public void removeCoins(UUID uuid, long coins, String serverName, boolean send) {
        ICoinUser coinUser = getCoinUser(uuid);
        if (coinUser == null) return;

        if (!send) {
            if (serverName.equals(CloudNetDriver.getInstance().getComponentName())) return;
            coinUser.removeCoins(coins);
            return;
        }
        coinUser.removeCoins(coins);
        removeCoins(uuid, coins, serverName);
        this.mySQL.update("UPDATE coinUser SET coins='" + coinUser.getCoins() + "' WHERE uuid='" + coinUser.getUUID().toString() + "'");
    }

    private void removeCoins(UUID uuid, long coins, String serverName) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        jsonDocument.append("coins", coins);
        jsonDocument.append("serverName", serverName);

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("removeCoins", "update", jsonDocument);
    }

    private void loadCoinUsers() {
        ResultSet result = this.mySQL.getResult("SELECT * FROM coinUser");
        while (true) {
            try {
                if (!result.next()) break;

                UUID uuid = UUID.fromString(result.getString("uuid"));
                long coins = result.getLong("coins");

                ICoinUser coinUser = new CoinUser(uuid, coins);
                this.coinUserMap.put(uuid, coinUser);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
